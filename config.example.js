export const
    /**
     * HTTP server port
     * @type {number}
     */
    port = undefined,
    /**
     * Strapi URL
     * @type {string}
     */
    strapiUrl = undefined,
    /**
     * Strapi API key
     * @type {string}
     */
    strapiApiKey = undefined;